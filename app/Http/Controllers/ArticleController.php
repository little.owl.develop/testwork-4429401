<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if (!$request->ajax()) abort(404);
        $perPage = 5;
        return Article::query()->latest()->orderBy('id', 'desc')->paginate($perPage);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $article = new Article([
            'title' => $request->input('title'),
            'detail' => $request->input('detail'),
        ]);
        $article->save();

        return response()->json('Article created');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $article = Article::find($id);
        return response()->json($article);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $article = Article::find($id);
        $article->update($request->all());

        return response()->json('Article updated');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        return response()->json('Article deleted');
    }
}
