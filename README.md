# Laravel 8 Vue CRUD project

## Установка

1. Копировать **.env.example** в **.env**, изменить нужные параметры (например, подключение к базе данных)
2. ```composer install```
3. ```npm install```
4. ```php artisan key:generate```
5. ```npm run dev``` или ```npm run watch```
6. ```php artisane migrate```
7. ```php artisane db:seed``` заполнение базы данных по желанию
8. ```php artisan ide-helper:generate``` для удобства разработки по желанию
